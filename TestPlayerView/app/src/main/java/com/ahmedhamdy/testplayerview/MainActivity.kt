package com.ahmedhamdy.testplayerview


import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

import android.view.WindowManager
import android.widget.Toast
import androidx.core.content.ContextCompat

import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.tonyodev.fetch2.Download
import com.tonyodev.fetch2.Request
import com.tonyodev.fetch2core.FetchObserver
import com.tonyodev.fetch2core.Reason
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File

//https://edugraminovaeg.s3.eu-central-1.amazonaws.com/market_place/staging/J3ee9frL4z8XoaxYQmkktA_1631126248_.mp4?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA2YWGCW7NOQZ2V2G3%2F20210909%2Feu-central-1%2Fs3%2Faws4_request&X-Amz-Date=20210909T161047Z&X-Amz-Expires=10800&X-Amz-SignedHeaders=host&X-Amz-Signature=b89218addff7d3a5e7dc6e354b1a8bc05fd2b486e1bfb1e58adb1b0aea2fbbd2
//https://edugraminovaeg.s3.eu-central-1.amazonaws.com/market_place/staging/J3ee9frL4z8XoaxYQmkktA_1631126248_MCtranscode.m3u8
//https://edugraminovaeg.s3.eu-central-1.amazonaws.com/market_place/staging/J3ee9frL4z8XoaxYQmkktA_1631126248_.mp4

//https://edugraminovaeg.s3.eu-central-1.amazonaws.com/market_place/staging/SPU6iLu7lqm3t_VERxB-hQ_1629311621_.mp4
private const val TAG = "MainActivity"
class MainActivity : AppCompatActivity(), FetchObserver<Download> {

    val URL = "https://storage.googleapis.com/exoplayer-test-media-0/BigBuckBunny_320x180.mp4"
    val URL2 = "https://i.imgur.com/7bMqysJ.mp4"
    //"https://edugraminovaeg.s3.eu-central-1.amazonaws.com/market_place/staging/J3ee9frL4z8XoaxYQmkktA_1631126248_.mp4?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA2YWGCW7NOQZ2V2G3%2F20210908%2Feu-central-1%2Fs3%2Faws4_request&X-Amz-Date=20210908T205425Z&X-Amz-Expires=10800&X-Amz-SignedHeaders=host&X-Amz-Signature=13e540012485f357d09eda2ead2f87365948dee5f73f2622d6eefcaecba0108"
    val URL3 = "https://www.youtube.com/watch?v=j1cM0X1zG2U"

    val sss = "https://edugraminovaeg.s3.eu-central-1.amazonaws.com/market_place/staging/J3ee9frL4z8XoaxYQmkktA_1631126248_.mp4?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIA2YWGCW7NOQZ2V2G3%2F20210909%2Feu-central-1%2Fs3%2Faws4_request&X-Amz-Date=20210909T161047Z&X-Amz-Expires=10800&X-Amz-SignedHeaders=host&X-Amz-Signature=b89218addff7d3a5e7dc6e354b1a8bc05fd2b486e1bfb1e58adb1b0aea2fbbd2"

    private val fetch = MyApp.fetch
    private lateinit var request: Request
    private var downloadId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*val player = SimpleExoPlayer.Builder(this).build()

        playerView.player = player

        val mediaItem = MediaItem.fromUri("https://storage.googleapis.com/exoplayer-test-media-0/BigBuckBunny_320x180.mp4")
        player.setMediaItem(mediaItem)
        player.prepare()
        player.play()
        */

//File("/home/ahmed/here.txt").writeText("sddddddddddddd")

        /*val path = this.getExternalFilesDir(null)
        val letDirectory = File(path, "LET")
        letDirectory.mkdirs()

        val file = File(letDirectory, "Records.txt")
        file.appendText("record goes here")


        val inputAsString = FileInputStream(file).bufferedReader().use { it.readText() }
        Toast.makeText(this, inputAsString, Toast.LENGTH_LONG).show()*/
        // make activity full screen


        val videosDir = File(this.filesDir, "savedVideosDir")
        if (!videosDir.exists()) {
            videosDir.mkdir()
        }

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN
        ,WindowManager.LayoutParams.FLAG_FULLSCREEN)

        val filePath = ContextCompat.getExternalFilesDirs(applicationContext, null)[0].absolutePath + "/" + Util.getNameFromUrl("https://i.imgur.com/7bMqysJ.mp4")
        //Toast.makeText(this, file, Toast.LENGTH_LONG).show()
        val file = File(filePath)
        val uri: Uri
        //val file = File(videosDir, Utils.getNameFromUrl(Utils.URL))
        if (file.exists()) {
            Toast.makeText(this, "saved", Toast.LENGTH_SHORT).show()
             uri = Uri.parse(filePath)
        }else{
            uri = Uri.parse("https://i.imgur.com/7bMqysJ.mp4")
            enqueueDownload()
        }
        // video url

        // load control
        val loadControl = DefaultLoadControl()
        // band width meter
        val bandwidthMeter = DefaultBandwidthMeter()

        val trackSelector = DefaultTrackSelector()
        // simple exoplayer
        val player = ExoPlayerFactory.newSimpleInstance(this,trackSelector, loadControl)

        // datasource factory
        val factory = DefaultHttpDataSourceFactory("exoplayer_video")
        // extractor factory
        val extractorFactory = DefaultExtractorsFactory()
        // mediaSource
        val mediaSource = ExtractorMediaSource(uri, DefaultDataSourceFactory(this, "exoplayer_video"), extractorFactory, null, null)

        // set player
        playerView.player = player
        playerView.keepScreenOn = true
        player.prepare(mediaSource)
        player.playWhenReady = true




    }
    private fun enqueueDownload() {
           val url ="https://i.imgur.com/7bMqysJ.mp4"
//        //val filePath: String = Data.getSaveDir(this).toString() + "/movies/" + Data.getNameFromUrl(url)
          val filePath = Util.getRootDirPath(this) + "/" + Util.getNameFromUrl(url)
        request = Request(url, filePath)
        //request.extras = getExtrasForRequest(request)
        fetch.attachFetchObserversForDownload(request.id, this)
            .enqueue(request, {
                    result -> request = result

            }) { error ->
                Log.d(
                    TAG,
                    "DownloadActivity Error: $error"
                )
            }
    }

    override fun onChanged(data: Download, reason: Reason) {}

}

/*

val player = SimpleExoPlayer.Builder(this).build()

        playerView.player = player

        val mediaItem = MediaItem.fromUri("https://storage.googleapis.com/exoplayer-test-media-0/BigBuckBunny_320x180.mp4")
        player.setMediaItem(mediaItem)
        player.prepare()
        player.play()
 */

//File("/home/ahmed/here.txt").writeText("sddddddddddddd")

/*val path = this.getExternalFilesDir(null)
val letDirectory = File(path, "LET")
letDirectory.mkdirs()

val file = File(letDirectory, "Records.txt")
file.appendText("record goes here")


val inputAsString = FileInputStream(file).bufferedReader().use { it.readText() }
Toast.makeText(this, inputAsString, Toast.LENGTH_LONG).show()
// make activity full screen
window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN
,WindowManager.LayoutParams.FLAG_FULLSCREEN)

// video url
val uri = Uri.parse("https://i.imgur.com/7bMqysJ.mp4")

// load control
val loadControl = DefaultLoadControl()
// band width meter
val bandwidthMeter = DefaultBandwidthMeter()

val trackSelector = DefaultTrackSelector()
// simple exoplayer
val player = ExoPlayerFactory.newSimpleInstance(this,trackSelector, loadControl)

// datasource factory
val factory = DefaultHttpDataSourceFactory("exoplayer_video")
// extractor factory
val extractorFactory = DefaultExtractorsFactory()
// mediaSource
val mediaSource = ExtractorMediaSource(uri, factory, extractorFactory, null, null)

// set player
playerView.player = player
playerView.keepScreenOn = true
player.prepare(mediaSource)
player.playWhenReady = true*/