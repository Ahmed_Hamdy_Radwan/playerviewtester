package com.ahmedhamdy.testplayerview

import android.content.Context
import android.net.Uri
import android.os.Environment
import androidx.core.content.ContextCompat
import java.io.File
import java.util.*

object Util {

    fun getProgressDisplayLine(currentBytes: Long, totalBytes: Long): String? {
        return getBytesToMBString(currentBytes) + "/" + getBytesToMBString(totalBytes)
    }

    private fun getBytesToMBString(bytes: Long): String {
        return String.format(Locale.ENGLISH, "%.2fMb", bytes / (1024.00 * 1024.00))
    }

    fun getRootDirPath(context: Context): String{
        return if(Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()){
            val file = ContextCompat.getExternalFilesDirs(context.applicationContext, null)[0]
            file.absolutePath
        }else{
            context.applicationContext.filesDir.absolutePath
        }
    }
    fun getNameFromUrl(url: String?): String? {
        return Uri.parse(url).lastPathSegment
    }

}