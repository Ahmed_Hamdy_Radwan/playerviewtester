package com.ahmedhamdy.testplayerview.encryption

import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.security.InvalidAlgorithmParameterException
import java.security.InvalidKeyException
import java.security.NoSuchAlgorithmException
import java.security.spec.AlgorithmParameterSpec
import javax.crypto.Cipher
import javax.crypto.CipherOutputStream
import javax.crypto.NoSuchPaddingException
import javax.crypto.SecretKey

class Encryptor {

    private val DEFAULT_READ_WRITE_BLOCK_BUFFER_SIZE: Int = 1024
    private val ALGO_VIDEO_ENCRYPTOR = "AES/CBC/PKCS5Padding"

    @SuppressWarnings("resource")
    fun encrypt(
        key: SecretKey,
        paramSpec: AlgorithmParameterSpec,
        inputStream: InputStream,
        outputStream: OutputStream
    ): OutputStream
    {
        try{
            var outputStream = outputStream
            val cipher = Cipher.getInstance(ALGO_VIDEO_ENCRYPTOR)
            cipher.init(Cipher.ENCRYPT_MODE, key, paramSpec)
            outputStream = CipherOutputStream(outputStream, cipher)


            val buffer = byteArrayOf()
            var count = inputStream.read(buffer)
            while (count >= 0){
                outputStream.write(buffer, 0, count)
                count = inputStream.read(buffer)
            }
            outputStream.close()
            return outputStream
        }catch (algoException: NoSuchAlgorithmException){
            throw algoException
        }catch (ioException: IOException){
            throw ioException
        }catch (paddException: NoSuchPaddingException){
            throw paddException
        }catch (keyException: InvalidKeyException){
            throw keyException
        }catch (ex: InvalidAlgorithmParameterException){
            throw ex
        }
    }

    @SuppressWarnings("resource")
    fun decrypt(
        key: SecretKey,
        paramSpec: AlgorithmParameterSpec,
        inputStream: InputStream,
        encryptedPath: String
    ){
        try{

            val cipher = Cipher.getInstance(ALGO_VIDEO_ENCRYPTOR)
            cipher.init(Cipher.ENCRYPT_MODE, key, paramSpec)


        }catch (algoException: NoSuchAlgorithmException){
            throw algoException
        }catch (ioException: IOException){
            throw ioException
        }catch (paddException: NoSuchPaddingException){
            throw paddException
        }catch (keyException: InvalidKeyException){
            throw keyException
        }catch (ex: InvalidAlgorithmParameterException){
            throw ex
        }
    }

}