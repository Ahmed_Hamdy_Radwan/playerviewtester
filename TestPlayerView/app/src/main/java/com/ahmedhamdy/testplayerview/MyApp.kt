package com.ahmedhamdy.testplayerview

import android.app.Application
import android.content.Context
import com.tonyodev.fetch2.*
import com.tonyodev.fetch2core.Downloader
import com.tonyodev.fetch2rx.RxFetch

class MyApp: Application() {
    override fun onCreate() {
        super.onCreate()

        context = this

        val fetchConfig = FetchConfiguration.Builder(this)
            .enableRetryOnNetworkGain(true)
            .setDownloadConcurrentLimit(3)
            .setHttpDownloader(HttpUrlConnectionDownloader(Downloader.FileDownloaderType.PARALLEL))
            .setNotificationManager(object : DefaultFetchNotificationManager(this) {
                override fun getFetchInstanceForNamespace(namespace: String): Fetch {
                    return fetch
                }

                override fun shouldCancelNotification(downloadNotification: DownloadNotification): Boolean {
                    if (downloadNotification.status == Status.DELETED) {
                        fetch.delete(downloadNotification.notificationId)
                        return true;
                    } else {
                        return super.shouldCancelNotification(downloadNotification)
                    }
                }

            })
            .build()

        Fetch.Impl.setDefaultInstanceConfiguration(fetchConfig)
        RxFetch.Impl.setDefaultRxInstanceConfiguration(fetchConfig)

        fetch = Fetch.Impl.getDefaultInstance()


    }

    companion object {

        lateinit var fetch: Fetch
        private lateinit var context: MyApp

        fun getContext(): Context {
            return context
        }
    }

}